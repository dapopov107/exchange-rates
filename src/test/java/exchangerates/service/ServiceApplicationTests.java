package exchangerates.service;

import DTOClasses.RequestData;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class ServiceApplicationTests {

	@Test
	void contextLoads() {
	}
	
	CurrencyRepository currencyRepository = new CurrencyRepository();
	CurrencyService currencyService = new CurrencyService();
	RequestData requestData = new RequestData();

	@Test
	void testExchangeRates(){
		requestData.setBik("123");
		requestData.setFirstCharCode("USD");
		requestData.setSecondCharCode("EUR");
		assertEquals(currencyService.doExchangeRates(requestData).getBik(),"123");
	}

	@Test
	void testGetRates(){
		assertEquals(currencyService.getRates().get(0).getCharCode(),"AUD");
	}


}
