package exchangerates.service;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.IOException;
import java.util.logging.LogManager;
import java.util.logging.Logger;

@EnableScheduling
@SpringBootApplication
public class ServiceApplication {
	public static final Logger log = Logger.getLogger(ServiceApplication.class.getName());

	public static final String LOG_PROPERTIES_FILE = "/log.properties";
	private static final String MSG_SERVICE_START = "Service start";

	public static void main(String[] args) throws IOException {
		LogManager.getLogManager().readConfiguration(
				ServiceApplication.class.getResourceAsStream(LOG_PROPERTIES_FILE));
		log.info(MSG_SERVICE_START);
		SpringApplication.run(ServiceApplication.class, args);
	}
}
