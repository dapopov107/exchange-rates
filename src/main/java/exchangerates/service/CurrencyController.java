package exchangerates.service;

import DTOClasses.AnswerData;
import DTOClasses.RequestData;
import generated.ValCurs;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@RestController
public class CurrencyController {
    public static final Logger log = Logger.getLogger(CurrencyController.class.getName());
    private static final String MSG_GET_EXCHANGE_RATES = "Get exchange rates";
    private static final String MSG_GET_RATES = "Get Rates";
    private final CurrencyService currencyService = new CurrencyService();

    @GetMapping("/get")
    public List<ValCurs.Valute> getRates(){
        log.info(MSG_GET_RATES);
       return currencyService.getRates();
    }

    @PostMapping("/post")
    public AnswerData doExchangeRates(@RequestBody RequestData requestData){
        log.info(MSG_GET_EXCHANGE_RATES);
        return currencyService.doExchangeRates(requestData);
    }
}
