package exchangerates.service;

import DTOClasses.AnswerDataFactory;
import generated.ValCurs;
import lombok.NoArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

@Repository
@NoArgsConstructor
public class CurrencyRepository {
    private static final String XML_WITH_DATA = "http://www.cbr.ru/scripts/xml_daily.asp";
    private static final String MSG_START_UNMARSHALING = "Start unmarshaling";
    private static final String MSG_UPDATE_RATES = "Update Rates";
    private static final String MSG_EXCEPTION = "Exception in getCurrencyData method";
    private final AnswerDataFactory answerDataFactory = new AnswerDataFactory();
    public static final Logger log = Logger.getLogger(CurrencyRepository.class.getName());

    private ValCurs valCurs = getCurrencyData();

    public AnswerDataFactory getAnswerDataFactory() {
        return answerDataFactory;
    }

    public ValCurs getValCurs() {
        return valCurs;
    }

    @Scheduled(fixedRate = 600000)
    @Async
    private void setValCurs(){
        log.info(MSG_UPDATE_RATES);
        valCurs = getCurrencyData();
    }

    private ValCurs getCurrencyData() {
        try{
            JAXBContext context = JAXBContext.newInstance(ValCurs.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            URL url = new URL(XML_WITH_DATA);
            log.info(MSG_START_UNMARSHALING);
            return  (ValCurs) unmarshaller.unmarshal(url);
        }
        catch (JAXBException|MalformedURLException ex){
            log.info(MSG_EXCEPTION);
            return new ValCurs();
        }
    }
}
