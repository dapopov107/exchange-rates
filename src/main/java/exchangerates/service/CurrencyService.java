package exchangerates.service;

import DTOClasses.AnswerData;
import DTOClasses.RequestData;
import generated.ValCurs;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;


import java.util.*;
import java.util.logging.Logger;

@Service
@NoArgsConstructor
public class CurrencyService {
    public static final Logger log = Logger.getLogger(CurrencyService.class.getName());
    private static final String MSG_GET_NOW_RATES = "Get now rates";
    private static final String MSG_FIRST_CURRENCY_FOUND = "First currency found";
    private static final String MSG_SECOND_CURRENCY_FOUND = "Second currency found";
    private static final String MSG_VALUE_CALCULATION = "Value Calculation";
    private static final String MSG_SENDING_A_RESPONSE = "Sending a response";
    private static final String RESPONSE_TO_A_REQUEST = "Forming a response to a request";
    private static final String MSG_RESPONSE_TO_A_REQUEST1 = RESPONSE_TO_A_REQUEST;
    private static final String EMPTY_VALUE = "";
    private static final String COMMA = ",";
    private static final String POINT = ".";
    private static final String MSG_CREATE_ANSWER = "Create answer data for response";
    private final CurrencyRepository currencyRepository = new CurrencyRepository();
    private final Date date = new Date();

    public List<ValCurs.Valute> getRates() {
        log.info(MSG_GET_NOW_RATES);
        return currencyRepository.getValCurs().getValute();
    }

    public AnswerData doExchangeRates(RequestData requestData) {
        System.out.println(requestData.toString());
        log.info(MSG_RESPONSE_TO_A_REQUEST1);
        AnswerData answerData = currencyRepository.getAnswerDataFactory().getNewAnswerData();
        String firstValue = EMPTY_VALUE;
        String secondValue = EMPTY_VALUE;
        String firstName = EMPTY_VALUE;
        String secondName = EMPTY_VALUE;
        log.info(MSG_GET_NOW_RATES);
        List<ValCurs.Valute> valuteList = currencyRepository.getValCurs().getValute();
        for (ValCurs.Valute currency: valuteList){
            if (currency.getCharCode().equals(requestData.getFirstCharCode())) {
                log.info(MSG_FIRST_CURRENCY_FOUND);
                firstValue = currency.getValue().replace(COMMA, POINT);
                firstName = currency.getName();
            }
            if (currency.getCharCode().equals(requestData.getSecondCharCode())) {
                log.info(MSG_SECOND_CURRENCY_FOUND);
                secondName = currency.getName();
                secondValue = currency.getValue().replace(COMMA, POINT);
            }

        }
        log.info(MSG_VALUE_CALCULATION);
        String exchangeValue = Double.toString(Double.parseDouble(firstValue)
                / Double.parseDouble(secondValue));
        log.info(MSG_CREATE_ANSWER);
        answerData.setReqID(UUID.randomUUID().toString());
        answerData.setFirstName(firstName);
        answerData.setSecondName(secondName);
        answerData.setBik(requestData.getBik());
        answerData.setFirstCharCode(requestData.getFirstCharCode());
        answerData.setSecondCharCode(requestData.getSecondCharCode());
        answerData.setReqDate(date);
        answerData.setValue(exchangeValue);
        log.info(MSG_SENDING_A_RESPONSE);
        return answerData;
    }
}
