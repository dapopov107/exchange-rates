package DTOClasses;

import lombok.NoArgsConstructor;

//фабрика для генерации ответов на запрос
@NoArgsConstructor
public class AnswerDataFactory {
    public AnswerData getNewAnswerData(){
        return new AnswerData();
    }
}
