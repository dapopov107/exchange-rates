package DTOClasses;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
//DTO, предназначенное для хранения данных на запрос

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestData {
    @Override
    public String toString() {
        return "RequestData{" +
                "bik='" + bik + '\'' +
                ", firstCharCode='" + firstCharCode + '\'' +
                ", secondCharCode='" + secondCharCode + '\'' +
                '}';
    }

    private String bik;
    private String firstCharCode;
    private String secondCharCode;

    public String getBik() {
        return bik;
    }

    public void setBik(String bik) {
        this.bik = bik;
    }

    public String getFirstCharCode() {
        return firstCharCode;
    }

    public void setFirstCharCode(String firstCharCode) {
        this.firstCharCode = firstCharCode;
    }

    public String getSecondCharCode() {
        return secondCharCode;
    }

    public void setSecondCharCode(String secondCharCode) {
        this.secondCharCode = secondCharCode;
    }
}
